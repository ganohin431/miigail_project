extends Control


func _on_start_button_pressed():
	TransitionPlayer.change_scene("res://scenes/levels/outside/outside.tscn")


func _on_exit_button_pressed():
	TransitionPlayer.exit()
