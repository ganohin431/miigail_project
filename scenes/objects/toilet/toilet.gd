extends ItemContainer

func hit():
	if not opened:
		$LidSprite.hide()
		var pos = $SpawnPosition.get_children()[0].global_position
		open.emit(pos, current_direction)
		opened = true
		$AudioStreamPlayer2D.play()
